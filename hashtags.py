from sys import exit
import click
import requests

def fetch_toots(session, base_url, token, limit, tag):
    path = 'timelines/tag/{}'.format(tag)
    url = '{}/api/v1/{}'.format(base_url, path)
    headers = {
        'Authorization': 'Bearer {}'.format(token)
    }
    params = {
        'limit': limit
    }

    first_page = session.get(
        url,
        params=params,
        headers=headers
    )

    if first_page.status_code != 200:
        raise StopIteration()

    yield first_page.json()
    last_page = first_page

    fetch_more = True
    while(fetch_more):
        toots = last_page.json()
        if not len(toots):
            raise StopIteration()

        if last_page.status_code != 200:
            raise StopIteration()

        params['max_id'] = toots[-1]['id']
        last_page = session.get(
            url,
            params=params,
            headers=headers
        )
        yield toots


@click.command()
@click.option(
    '--url',
    metavar='MASTODON_URL',
    envvar='MASTODON_URL',
    help='Mastodon instance base url: https://fediverse.tld'
)
@click.option(
    '--token',
    metavar='MASTODON_ACCESS_TOKEN',
    envvar='MASTODON_ACCESS_TOKEN',
    help='Mastodon access token'
)
@click.option(
    '-l', '--limit',
    default=5,
    help='Toots to request each time, max 40 in vanilla mastodon source.'
)
@click.option(
    '-m', '--max-toots',
    default=40,
    help='Max toots to request in total'
)
@click.option(
    '-t', '--tag',
    #multiple=True,
    required=True,
    help='Tag to search'
)
@click.option('-v', '--verbose', count=True)
@click.argument('search')
def search_hashtags(url, token, limit, max_toots, tag, verbose, search):
    if verbose: print('Searching (max {} toots)...'.format(max_toots))
    count_total_toots = 0
    session = requests.Session()

    # Paginate toots from API
    for toots in fetch_toots(session, url, token, limit, tag):
        count_total_toots += len(toots)
        if not len(toots): break
        for toot in toots:
            if search in toot['content']:
                print(toot['url'], toot['id'], toot['created_at'])

        # Exit if we request too many toots
        if count_total_toots >= max_toots:
            if verbose:
                print('Reached max toots searched ({}/{})'.format(
                    count_total_toots,
                    max_toots
                ))
            break

    if verbose > 1: print('Searched a total of {} toots'.format(
        count_total_toots))


if __name__ == '__main__':
    exit(search_hashtags(None))
