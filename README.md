# Mastodon crawler stuff

Just going to put random stuff related to Mastodon API crawling here.

## Install dependencies in venv

    python -m venv .venv
    source .venv/bin/activate
    pip install -r requirements.txt
    # run `deactivate` to exit venv

## hashtags.py

Search a specific hashtag for a string. Requires an access token with ``read:search`` and ``read:statuses`` permissions.

    python hashtags.py -t fediblock sexist

Check ``python hashtags.py --help`` for more info, url and token can be environment variables if you want to use [direnv](https://github.com/direnv/direnv).

### TODO

- [ ] Support some sort of pattern in search format, maybe fnmatch.
- [ ] Use bsoup to only search html tag content
